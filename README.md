Projet regroupant les scripts utilisés comme hooks pour `git`.

Permet notamment

- de rejeter les `rebases` de commits protégés (ex. les commits poussés de master)
- de rejeter les commits ajoutant des fichiers invalides
    - php (via `phpstan`)
    - yaml (via la commande symfony `lint:yaml`)
- de corriger automatiquement certaines erreurs après un commit, et de générer un commit `fixup` contenant ces corrections
    - les règles de codage de fichier php (via `php-cs-fixer`)
    - l'oublie de la mise à jour du `composer.lock` lorsque le `composer.json` est modifié
- de mettre à jour automatiquement les librairies utilisées lors d'un changement de branche lorsque c'est nécessaire

# Possibles améliorations

- [ ] utiliser `git config` pour configurer les hooks
- [ ] hook pre-commit
    - [ ] vérifier les fichiers twigs (sur les projets symfony multi-kernel)
    - [ ] vérifier la syntaxe des fichiers cron
